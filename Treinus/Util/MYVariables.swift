//
//  MYVariables.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

struct MyVariables {
    static let imageCache = AutoPurgingImageCache(memoryCapacity: 50000000, preferredMemoryUsageAfterPurge: 10000000)
}
