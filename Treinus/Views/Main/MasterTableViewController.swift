//
//  MasterTableViewController.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MapKit

class MasterTableViewController: UITableViewController, CLLocationManagerDelegate {

    var gyms = [Gym]()
    var locationManager = CLLocationManager()
    var coordenate: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
//        PlacesServeice.getPhotoReference(photo_reference: "CmRaAAAA8OEgp127g254mphD1XySyql8M2X_OiAK26tB4HZvf2wPLeaEQFi69Sf8toOaA9vJhBT4RNCcvgj3S_pvuZpjQB4uBekZjDPo9YtbtzcCMxDac1Pq7on9i64ufFBRTpR1EhBX88RwueVsAebsz8GOOeCYGhSSmtfnDcZkRrU-SPUHUYxQhqdujQ") { (image) in
//            
//        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status != .authorizedWhenInUse && status != .notDetermined {
            let alertController = UIAlertController(title: "Permissão de Localizção", message: "Precisamos da sua localização, para funcionar corretamente.", preferredStyle: .alert)
            
            let actionConf = UIAlertAction(title: "Abrir Configurações", style: .default) { (alertConf) in
                if let conf = NSURL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.open(conf as URL)
                }
            }
            
            let actionCancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
            
            alertController.addAction(actionConf)
            alertController.addAction(actionCancel)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordenate = locationManager.location?.coordinate {
            self.coordenate = coordenate
            self.configure()
        }
    }
    
    func configure() {
        if let coordinate = self.coordenate {
            PlacesServeice.getGyms(lat: String(coordinate.latitude), lng: String(coordinate.longitude)) { (gyms) in
                self.gyms = gyms
                
                for g in self.gyms {
                    if let lat = g.geometry?.location?.lat, let lng = g.geometry?.location?.lng {
                        let locationUser = CLLocation(latitude: lat, longitude: lng)
                        let locationGym = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                        let distance = locationUser.distance(from: locationGym)
                        g.distance = distance
                    }
                }
                self.gyms.sort(by: { $0.distance ?? 0 < $1.distance ?? 0 })
                self.tableView.reloadData()
            }
            
            PlacesServeice.getGym(place_id: "ChIJEcOnwMGXpgAR1lxhptSvZaE") { (gym) in
                print("Endereço: " + (gym.formatted_address ?? "Não encontrado"))
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gyms.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? GymTableViewCell {
            let gym = gyms[indexPath.row]
            cell.setGym(gym: gym)
            
            if let urlImage = gym.icon {
                let urlRequest = URLRequest(url: URL(string: urlImage)!)
                
                if let image = MyVariables.imageCache.image(for: urlRequest, withIdentifier: urlImage) {
                    cell.iconImageView.image = image
                }
                else {
                    Alamofire.request(urlImage).responseImage(completionHandler: { (response) in
                        if let image = response.result.value {
                            MyVariables.imageCache.add(image, for: urlRequest, withIdentifier: urlImage)
                            
                            DispatchQueue.main.async {
                                if let url = URL(string: urlImage) {
                                    cell.iconImageView.af_setImage(withURL: url)
                                }
                            }
                        }
                    })
                }
            }
            
            if let photo_reference = gym.photos?.first?.photo_reference {
                if let image = MyVariables.imageCache.image(withIdentifier: photo_reference) {
                    cell.gymImageView.image = image
                }
                else {
                    PlacesServeice.getPhotoReference(photo_reference: photo_reference) { (image) in
                        MyVariables.imageCache.add(image, withIdentifier: photo_reference)
                        
                        DispatchQueue.main.async {
                            cell.gymImageView.image = image
                        }
                    }
                }
            }
            
            return cell
        }

        return UITableViewCell()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGym" {
            if let indexPath = tableView.indexPathForSelectedRow {
                if let controller = (segue.destination as? GymViewController) {
                    let gym = self.gyms[indexPath.item]
                    controller.place_id = gym.place_id
                }
            }
        }
    }

}
