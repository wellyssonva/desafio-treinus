//
//  GymTableViewCell.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit

class GymTableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var gymImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var openNowLabel: UILabel!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.iconImageView.image = nil
        self.gymImageView.image = nil
        self.nameLabel.text = nil
        self.distanceLabel.text = nil
        self.ratingLabel.text = nil
        self.openNowLabel.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setGym(gym: Gym) {
        self.nameLabel.text = gym.name ?? ""
        self.ratingLabel.text = String(gym.rating ?? 0)
        if gym.distance ?? 0 > 1000 {
            self.distanceLabel.text = String(format: "%.2f", (gym.distance ?? 0) / 1000) + " Km"
        }
        else {
            self.distanceLabel.text = String(format: "%.2f", gym.distance ?? 0) + " Metros"
        }
        self.openNowLabel.text = (gym.openingHours?.open_now ?? false) ? "Sim" : "Não"
    }

}
