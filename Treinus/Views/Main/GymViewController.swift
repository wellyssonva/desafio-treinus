//
//  GymViewController.swift
//  Treinus
//
//  Created by Wellysson Avelar on 30/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import MapKit

class GymViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    var place_id: String?
    var locationManager = CLLocationManager()
    var cont = 0
    var gym: Gym?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.map.delegate = self
        self.locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()        
        self.loadPlace()
    }
    
    func loadPlace() {
        if let place_id = self.place_id {
            self.view.showBlurLoader()
            PlacesServeice.getGym(place_id: place_id) { (gym) in
                self.gym = gym
                self.nameLabel.text = gym.name
                self.addressLabel.text = gym.formatted_address
                self.view.removeBluerLoader()
                if let urlImage = gym.icon {
                    let urlRequest = URLRequest(url: URL(string: urlImage)!)
                    Alamofire.request(urlImage).responseImage(completionHandler: { (response) in
                        if let image = response.result.value {
                            MyVariables.imageCache.add(image, for: urlRequest, withIdentifier: urlImage)
                            
                            DispatchQueue.main.async {
                                if let url = URL(string: urlImage) {
                                    self.iconImageView.af_setImage(withURL: url)
                                }
                            }
                        }
                    })
                }
                
                if let lat = gym.geometry?.location?.lat, let lng = gym.geometry?.location?.lng {
                    self.setGymMap(lat: lat, lng: lng)
                }
            }
        }
    }
    
    func setGymMap(lat: Double, lng: Double) {
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = self.gym?.name
        
        self.map.addAnnotation(annotation)
        self.centerMap()
        setDirection()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func centerMap() {
        if let coordenate = locationManager.location?.coordinate {
            let distance = getDistance() * 2
            let region = MKCoordinateRegionMakeWithDistance(coordenate, distance, distance)
            map.setRegion(region, animated: true)
        }
    }
    
    func setDirection() {
        if let lat = self.gym?.geometry?.location?.lat, let lng = self.gym?.geometry?.location?.lng {
            let sourceCoordinate = self.locationManager.location?.coordinate
            let destinateCoordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            
            let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinate!)
            let destinatePlacemark = MKPlacemark(coordinate: destinateCoordinate)
            
            let sourceItem = MKMapItem(placemark: sourcePlacemark)
            let destinateItem = MKMapItem(placemark: destinatePlacemark)
            
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = sourceItem
            directionRequest.destination = destinateItem
            directionRequest.transportType = .automobile
            
            let directions = MKDirections(request: directionRequest)
            directions.calculate(completionHandler: {
                response, error in
                guard let response = response else {
                    if let error = error {
                        print(error)
                    }
                    return
                }
                let route = response.routes[0]
                self.map.add(route.polyline, level: .aboveRoads)
                
                let rekt = route.polyline.boundingMapRect
                self.map.setRegion(MKCoordinateRegionForMapRect(rekt), animated: true)
                
            })
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay)
        render.strokeColor = UIColor.blue
        render.lineWidth = 1.5
        
        return render
    }
    
    func getDistance() -> Double {
        if let lat = self.gym?.geometry?.location?.lat, let lng = self.gym?.geometry?.location?.lng {
            let locationUser = self.locationManager.location
            let locationGym = CLLocation(latitude: lat, longitude: lng)
            let distance = locationUser?.distance(from: locationGym)
            return distance ?? 0
        }
        return 0
    }
    
    @IBAction func centerMapButton() {
        self.centerMap()
    }
    
    @IBAction func navigeteButton() {
        if let lat = self.gym?.geometry?.location?.lat, let lng = self.gym?.geometry?.location?.lng {
            let distance = self.getDistance()
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, distance, distance)
            
            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
            let placemark = MKPlacemark(coordinate: coordinate)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = self.gym?.name ?? ""
            mapItem.openInMaps(launchOptions: options)
            
        }
    }
}
