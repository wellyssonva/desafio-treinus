//
//  PlacesService.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class PlacesServeice {
    static var userKey = "AIzaSyAXJE40j6bz6fzAIS4UAOEGbjxXLblj6tA"
    
    static func getGyms(lat: String, lng: String, completion: @escaping ([Gym])->()) {
        Alamofire.request("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(lat),\(lng)&radius=50000&types=gym&key=\(userKey)").responseJSON { response in
            
            if let json = response.result.value as? [String: Any] {
                if let gyms = Mapper<Gym>().mapArray(JSONObject: json["results"]) {
                    completion(gyms)
                }
                else {
                    let gyms = [Gym]()
                    completion(gyms)
                }
            }
        }
    }
    
    static func getGym(place_id: String, completion: @escaping (Gym)->()) {
        Alamofire.request("https://maps.googleapis.com/maps/api/place/details/json?placeid=\(place_id)&key=\(self.userKey)").responseJSON { response in
            
            if let json = response.result.value as? [String: Any] {
                if let gym = Mapper<Gym>().map(JSONObject: json["result"]) {
                    completion(gym)
                }
            }
        }
    }
    
    static func getPhotoReference(photo_reference: String, completion: @escaping (UIImage)->()) {
        Alamofire.request("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=\(photo_reference)&key=\(self.userKey)").responseImage(completionHandler: { (response) in
            if let image = response.result.value {
                completion(image)
            }
        })
    }
}
