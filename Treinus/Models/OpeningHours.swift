//
//  OpeningHours.swift
//  Treinus
//
//  Created by Wellysson Avelar on 30/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class OpeningHours: Mappable {
    var open_now: Bool?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.open_now <- map["open_now"]
    }
    
}
