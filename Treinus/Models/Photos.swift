//
//  photos.swift
//  Treinus
//
//  Created by Wellysson Avelar on 01/05/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class Photos: Mappable {
    var height: Int?
    var html_attributions: String?
    var photo_reference: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.height <- map["height"]
        self.html_attributions <- map["html_attributions"]
        self.photo_reference <- map["photo_reference"]
    }
}

