//
//  Location.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class Location: Mappable {
    var lat: Double?
    var lng: Double?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.lat <- map["lat"]
        self.lng <- map["lng"]
    }
}
