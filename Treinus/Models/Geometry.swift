//
//  Geometry.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class Geometry: Mappable {
    var location: Location?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        self.location <- map["location"]
    }
}
