//
//  Gym.swift
//  Treinus
//
//  Created by Wellysson Avelar on 29/04/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class Gym: Mappable {
    var id: String?
    var name: String?
    var place_id: String?
    var icon: String?
    var rating: Double?
    var formatted_phone_number: String?
    var formatted_address: String?
    var geometry: Geometry?
    var openingHours: OpeningHours?
    var distance: Double?
    var photos: [Photos]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.place_id <- map["place_id"]
        self.icon <- map["icon"]
        self.rating <- map["rating"]
        self.geometry <- map["geometry"]
        self.formatted_phone_number <- map["formatted_phone_number"]
        self.formatted_address <- map["formatted_address"]
        self.openingHours <- map["opening_hours"]
        self.photos <- map["photos"]
    }
    
}
