////
////  JsonFile.swift
////  TreinusTests
////
////  Created by Wellysson Avelar on 01/05/2018.
////  Copyright © 2018 Wellysson Avelar. All rights reserved.
////
//
//import Foundation
//
//class JsonFile {
//    static let jsonRepository: [String: Any] = [
//    [
//        "geometry" : [
//            "location" : [
//                "lat" : -19.9621515,
//                "lng" : -43.9640514
//            ],
//            "viewport" : [
//                "northeast" : [
//                    "lat" : -19.9607907697085,
//                    "lng" : -43.9626704197085
//                ],
//                "southwest" : [
//                    "lat" : -19.96348873029151,
//                    "lng" : -43.9653683802915
//                ]
//            ]
//        ],
//        "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
//        "id" : "cf27483a2cc814fd6bba156904b52198f3335899",
//        "name" : "Centro Taffarel de Futebol",
//        "opening_hours" : [
//            "open_now" : true,
//            "weekday_text" : []
//        ],
//        "place_id" : "ChIJEcOnwMGXpgAR1lxhptSvZaE",
//        "rating" : 4.4,
//        "reference" : "CmRSAAAA0frbxyulJefiPQWsTqORyPSqMkmkdNMmjQKb4dt7nld1k4mRJHizqFWl9tPnBYPBiya1hdK7V1U1DeH78mwodYFNvq_Gp2vzRE20dSBhCs62IuZa99lKz7QYEE06TVPAEhArGRZUUJOiSb5Eh-EbVGC5GhRnYRVadctXrEFGzYsNUx6eYj2yYw",
//        "scope" : "GOOGLE",
//        "types" : [ "gym", "health", "point_of_interest", "establishment" ],
//        "vicinity" : "Avenida Barão Homem de Melo, 3040 - Buritis, Belo Horizonte"
//        ],
//    [
//        "geometry" : [
//            "location" : [
//                "lat" : -19.9495542,
//                "lng" : -44.0510679
//            ],
//            "viewport" : [
//                "northeast" : [
//                    "lat" : -19.9482065697085,
//                    "lng" : -44.0496466697085
//                ],
//                "southwest" : [
//                    "lat" : -19.9509045302915,
//                    "lng" : -44.0523446302915
//                ]
//            ]
//        ],
//        "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
//        "id" : "8d8d4bf0fa04705fbe077f9a51ce44e5d9cf285d",
//        "name" : "Academia Trainner",
//        "opening_hours" : [
//            "open_now" : true,
//            "weekday_text" : []
//        ],
//        "photos" : [
//        [
//        "height" : 480,
//        "html_attributions" : [
//        "\u003ca href=\"https://maps.google.com/maps/contrib/108381859362214493490/photos\"\u003eAlmerin Pereira\u003c/a\u003e"
//        ],
//        "photo_reference" : "CmRaAAAA7uRqgTWdvz_qjmUfuG6SrdjpmQru8CNhjlgGuFawGpzXnnywm_WgTEHwLcEMFdjOnZBzd1VXnBZpsCREDTYN00V2B95uDx4DpyMgkPW7ZkMd_gDFLJYiY5LdklK-cHmCEhDvS6vYw41rXW3OsK2BPR-0GhQn8VUZK0MEkDNAC17F2dkm2v79zQ",
//        "width" : 720
//        ]
//        ],
//        "place_id" : "ChIJM6pLCIeVpgARII-qoWntk8I",
//        "rating" : 4.6,
//        "reference" : "CmRSAAAADu-QiJD8Hkvm8ro2tDO7mTsRsYqAGr0AVuND8IBhMG3QkCMborB0FqcfwvaggKr0WKEYUhQuX7vv1nFStnrNIr_gnkxcLjnWqLj9ZlDiTlFa52Eygdgc3jgrDTIOsXedEhBkaeqI1kjFILhq0CuterJeGhQX9B52l9fGWaT3PhDu4Hx3AHuCIQ",
//        "scope" : "GOOGLE",
//        "types" : [ "gym", "health", "point_of_interest", "establishment" ],
//        "vicinity" : "Rua Rio Tocantins, 282 - Riacho das Pedras, Contagem"
//        ]]
//}
